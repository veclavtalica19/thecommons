extends Node

const c_size_dim := 16
const c_region_max_dispersion := 0.4
const c_region_min_elevation := -250
const c_region_max_elevation := 1250

var m_size := Vector2(c_size_dim, c_size_dim)
var m_seed = null
var m_region_tree := QuadTree.new(Rect2(Vector2.ZERO, m_size))

var _m_rng := RandomNumberGenerator.new()


func _init() -> void:
    if m_seed == null:
        _m_rng.randomize()
        m_seed = _m_rng.seed
    else:
        _m_rng.seed = int(m_seed)
    _generate_world()


func _generate_world() -> void:
    _generate_board()
    _generate_elevation()


func _generate_board() -> void:
    var delaunay := Delaunay.new(Rect2(Vector2.ZERO, m_size))

    for x in m_size.x:
        for y in m_size.y:
            var point = Vector2(
                x + _m_rng.randf_range(-c_region_max_dispersion, c_region_max_dispersion),
                y + _m_rng.randf_range(-c_region_max_dispersion, c_region_max_dispersion))
            point.x = clamp(point.x, 0, c_size_dim)
            point.y = clamp(point.y, 0, c_size_dim)
            m_region_tree.insert(point, Region.new())
            delaunay.add_point(point)

    for site in delaunay.make_voronoi(delaunay.triangulate()):
        m_region_tree.element_at_point(site.center).m_site = site


func _generate_elevation() -> void:
    for region in m_region_tree.query_elements(Rect2(Vector2.ZERO, m_size)):
        region.m_elevation = _m_rng.randi_range(c_region_min_elevation, c_region_max_elevation)
