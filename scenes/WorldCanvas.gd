extends Node2D

# In world space coordinates
export(Rect2) var m_camera: Rect2
export(NodePath) var m_world_path: NodePath

# todo: Do it on visual server update?
func _draw() -> void:
    var world := get_node(m_world_path)
  
    # todo: Reuse already created polygons
    for child in get_children():
        if child is Polygon2D:
            child.free()

    for region in world.m_region_tree.query_items(m_camera):
        var polygon = Polygon2D.new()
        polygon.polygon = region.m_site.polygon
        add_child(polygon)
