extends Reference
class_name Region

## World region descriptor
##
## Holds all the data about geographic and ecological composition
## of concrete part of the world

var m_site: Delaunay.VoronoiSite

## In meters, on average, relative to ocean level
var m_elevation: int
